import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { HomeComponent } from './pages/home/home.component';
import { ListadoInfoComponent } from './pages/listado-info/listado-info.component';
import { ManualComponent } from './pages/manual/manual.component';
import { TrackingComponent } from './pages/tracking/tracking.component';
import { PagoPaso2Component } from './pages/pago-paso2/pago-paso2.component';

const rutas:Routes = [
  {path:'home', component:HomeComponent},
  {path:'manual',component:ManualComponent},
  {path:'listado-info',component:ListadoInfoComponent},
  {path:'tracking',component:TrackingComponent},
  {path:'clientes',component:ClientesComponent},
  {path:'pago-paso2',component:PagoPaso2Component},
  {path:'**', pathMatch:'full', redirectTo:'home'}
]


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(rutas)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
