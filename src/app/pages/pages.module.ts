import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from '../components/components.module';
import { ManualComponent } from './manual/manual.component';
import { ListadoInfoComponent } from './listado-info/listado-info.component';
import { PagoPaso2Component } from './pago-paso2/pago-paso2.component';


@NgModule({
  declarations: [HomeComponent, ManualComponent, ListadoInfoComponent, PagoPaso2Component],
  imports: [
    CommonModule,
    ComponentsModule
  ]
})
export class PagesModule { }
