import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manual',
  templateUrl: './manual.component.html',
  styles: [
  ]
})
export class ManualComponent implements OnInit {

  public elegirDatosfacTEDSOL:boolean;
  public elegirDatosfactuTED:boolean;
  public mostarDatos:boolean;
  constructor( ) {
    this.mostarDatos = false;
    this.elegirDatosfacTEDSOL = false;
    this.elegirDatosfactuTED = false;
  }


  ngOnInit(): void {
  }
  onShowHide(){
    this.mostarDatos =true; 
  }
  onElegirFacTEDSOL(){
    this.elegirDatosfacTEDSOL =true;
    this.elegirDatosfactuTED =false;
  }
  onElegirFactuTED(){
    this.elegirDatosfactuTED =true;
    this.elegirDatosfacTEDSOL =false;
  }

}
